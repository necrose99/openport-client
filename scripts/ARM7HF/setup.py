#from distutils.core import setup
from setuptools import setup, find_packages

setup(name='openport',
      version='1.0',
      description='Official Openport Client',
      author='Jan De Bleser',
      author_email='jan@openport.io',
      url='https://openport.io',
      packages=['openport'],
     )